import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        int[] array = new int[]{34, 0, 0, 14, -563, 15, 0, 18, -5, 0, 0, 1, 20, 100, 0, 0, 0, 745, 876, 0, 0, 0 ,0, 0, 10000, -7593};  // Объявили используемый массив
        System.out.println ("Изначально:" + Arrays.toString(array));
        move(array);
        System.out.println ("После махинаций:" + Arrays.toString(array));

    }

    private static void move (int[] array) {

        for (int i = 0; i < array.length; ++i) {     //Устойчивое выражение, определяющее пробегание по массиву последовательно, первый член сравнения;
            for (int j = 0; j < array.length-1; ++j) {   //По сути, j будет бежать за i...наверное...должен.
                if (array[i] != 0 && array[j] == 0){      // Как оказалось, важно, чтобы именно первый индекс был неравен 0!! Запомнить
                    int temp = array[i];                  // Какое значение "кладем в коробку" неважно, важна изначальная установка индексов
                    array[i] = array[j];              // Если первый индекс будет установлен, как равный 0, эти самые нули сколлекционируются слева и числа будут стоять как попало. ВАЖНО
                    array[j] = temp;

                }

            }
        }

    }
    
}

